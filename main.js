const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto('http://oms.indexlivingmall.com:8080', { waitUntil: 'networkidle0', timeout: 60000});
  // username
  await page.focus('#root > div > div.container > div > div > div > div > div > form > div:nth-child(1) > input')
  await page.keyboard.type('a')
  //password
  await page.focus('#root > div > div.container > div > div > div > div > div > form > div:nth-child(2) > input')
  await page.keyboard.type('12345678')

  await page.click('#root > div > div.container > div > div > div > div > div > form > div.row > div.col > button.text-white.m-2.btn.btn-primary')

  await page.screenshot({ path: '1example_1.png' });
//  console.log('done')
  console.log('another log')

//   await browser.close();
})();
